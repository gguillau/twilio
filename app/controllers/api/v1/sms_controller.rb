class Api::V1::SmsController < TwilioController
    
    def create
        render :json => ::MessagesService.new.reply(params)
    end
end