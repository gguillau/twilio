class Api::V1::StatusController < TwilioController
    
    def create
        render :status => 204
    end
end