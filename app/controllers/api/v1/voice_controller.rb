class Api::V1::VoiceController < TwilioController
    
    def create
        render :xml => VoiceService.new.reply(params).to_xml
    end
end