require 'twilio-ruby'

class MessagesService
  
    def load_client
        # put your own credentials here
        account_sid = "ACd25cb21f3590e614e3a1b801f3ace52d"
        auth_token = "ebd08207d53e77a501cba078a211e807"

        # set up a client to talk to the Twilio REST API
        @client = Twilio::REST::Client.new account_sid, auth_token
    end
    
    def send_message(number)
        
        load_client
        
        @client.messages.create(
            from: "+17812143567",
            to: number,
            body: "This is ZAADDDDY texting you from his conference. SEND NUDES"
        )
    end
    
    def second_message
        
        load_client
        
        @client.messages.create(
            from: "+17812143567",
            to: "+12162622522",
            body: "Did you know my favorite historical figure, JFK, died 54 years ago?",
            media_url: "https://peopledotcom.files.wordpress.com/2016/08/jfk-600.jpg?w=600"
        )
    end
    
    def reply(params)
        # get the details
        from = params["From"]
        array = params["Body"].split
        
        load_client
        
        # set the task and time
        task = array.first
        time = array.second.to_i
        
        # send the message
        MessagesService.new.delay(run_at: time.minutes.from_now).later(from, task)
        
        @client.messages.create(
            from: "+17812143567",
            to: from,
            body: "You just set up an appointment!",
            status_callback: "https://twilio-girgalicious.c9users.io/api/v1/status"
        )
    end
    
    def later(from, task)
        
        load_client
        
        @client.messages.create(
            from: "+17812143567",
            to: from,
            body: "Hey! You told me to remind you about '#{task}'",
            status_callback: "https://twilio-girgalicious.c9users.io/api/v1/status"
        )
    end

end