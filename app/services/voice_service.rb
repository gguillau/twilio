require 'twilio-ruby'

class VoiceService
  
    def load_client
        # put your own credentials here
        account_sid = "ACd25cb21f3590e614e3a1b801f3ace52d"
        auth_token = "ebd08207d53e77a501cba078a211e807"

        # set up a client to talk to the Twilio REST API
        @client = Twilio::REST::Client.new account_sid, auth_token
    end
    
    def call(number)
        
        load_client
        
        @call = @client.calls.create(
            from: "+17812143567",
            to: number,
            url: "https://twilio-girgalicious.c9users.io/api/v1/status"
        )
    end
    
    
    def reply(params)
        Twilio::TwiML::Response.new do |r|
            r.Dial "+17814928454"
        end
    end


end