# config/initializers/delayed_job_issue.rb
# RAILS 5 patches for delayed_job
# TODO: REMOVE WHEN upstream is updated
# https://github.com/collectiveidea/delayed_job/issues/944

module DelayedWorkerPatches
  def reload!
    return unless self.class.reload_app?
    if defined?(ActiveSupport::Reloader)
      Rails.application.reloader.reload!
    else
      ActionDispatch::Reloader.cleanup!
      ActionDispatch::Reloader.prepare!
    end
  end
end

module Delayed
  class Worker
    prepend DelayedWorkerPatches
  end
end