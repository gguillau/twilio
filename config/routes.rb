Twilio::Application.routes.draw do
  
    namespace :api do
        namespace :v1 do
            resources :sms
            resources :status
            resources :voice
        end
    end
    
    root :to => "home#index"
end