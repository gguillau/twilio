(function() {
    "use strict";

    angular
        .module("twilio.landing", [
            'ui.router'
        ])
        .config(['$stateProvider', landingConfig])
        .controller("LandingController", [
            LandingController
        ]);

    function LandingController() {
    }

    LandingController.prototype = {


    };

    function landingConfig($stateProvider) {
        $stateProvider
            .state('root', {
                url: '^/',
                templateUrl: 'landing/landing.tpl.html',
                controller: "LandingController",
                controllerAs: "ctrl"
            });
    }
})();