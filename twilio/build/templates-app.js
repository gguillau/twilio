angular.module('templates-app', ['landing/landing.tpl.html']);

angular.module("landing/landing.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("landing/landing.tpl.html",
    "<div class=\"container-fluid\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-12\">\n" +
    "            <h1 class=\"text-center\">Hello Twilio World!</h1>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);
