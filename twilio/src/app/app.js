angular.module('twilio', [
        'templates-app',
        'templates-common',
        'twilio.landing',
        'ui.router'
    ])
    .config(['$locationProvider', '$urlRouterProvider', legendsAppConfig])
    .controller('AppCtrl', function AppCtrl($scope, $location) {

    })
    .run(['$urlMatcherFactory', function run($urlMatcherFactory) {


        $urlMatcherFactory.caseInsensitive(true);
        $urlMatcherFactory.strictMode(false);


    }]);



function legendsAppConfig($locationProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    // use the HTML5 History API
    $locationProvider.html5Mode(true);

}